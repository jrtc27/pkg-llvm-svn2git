# pkg-llvm-svn2git

The collection of scripts and configuration files in this repository allow
pkg-llvm's active SVN repositories to be converted to git repositories, namely:

* emscripten
* iwyu
* libcxx
* llvm-defaults
* llvmlite
* llvm-toolchain
* openmprtl

## Steps

1. Clone this repository to a location herein referred to as `DIR`

2. Install subversion, git and svn-all-fast-export

3. Run the following commands in order:

   ```
   ./do-sync
   ./do-export
   ./do-fixup
   ./do-repack
   ```

The final git repositories are then located in `DIR/git/foo.git`, with
`DIR/git/foo.git.orig` containing all the pre-rewrite commits in case they are
required.

### do-sync

This will create a local mirror of svn://anonscm.debian.org/svn/pkg-llvm in
`DIR/svn/pkg-llvm`. If re-running, remove `DIR/svn` first.

### do-export

This simply runs `svn-all-fast-export` using the authors mapping and rules in
this repository, creating git repositories inside `DIR/git`. If re-running,
remove `DIR/git` first.

### do-fixup

This performs several rewriting steps to tidy up the history. Detached tags are
recreated to point to the right commit, superfluous merges are removed, and
subdirectory tags are reconstructed to point to a complete tree (some
repositories have their tags containing the contents of the `debian`
directory). If re-running, remove `DIR/git` and re-run `do-export` first, as
this modifies the repositories in-place.

### do-repack

This moves each repository `DIR/git/foo` to `DIR/git/foo.git.orig`, creates a
new bare clone in `DIR/git/foo.git` without all the pre-rewrite commits and
repacks.

### do-authors

This was used to generate the list of SVN authors from the history, and will
overwrite `authors.txt`. If re-running, full names and email addresses need to
be looked up manually to complete the regenerated file.
